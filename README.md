# Marley Spoon Coding Challenge

## Screenshots

Recipes View

![2021-07-19_at_8.05_PM](/uploads/3c757f5dca65ee5f981cd620e9a7289b/2021-07-19_at_8.05_PM.png)

Recipes Detail View

![2021-07-19_at_8.42_PM](/uploads/bba627d2fca109ec2aa74ead348526a0/2021-07-19_at_8.42_PM.png)

## Todos

- Modify Recipes View so it appears better on mobile
- Add a fade up animation to Recipe cards in Receipes View

## Nice to haves

- Give users the ability to upvote a recipe
- Give users to the ability to leave a comment so they can interact with the chef and other users

## Installation

Clone the repo

```
git clone git@gitlab.com:nisanth074/marley_spoon_challenge.git
cd marley_spoon_challenge/
```

Install the latest ruby https://rvm.io/

Install bundler

```
gem install bundler
```

Install gems

```
bundle install
```

Start the rails server

```
bundle exec rails server
```

Visit http://localhost:3000
