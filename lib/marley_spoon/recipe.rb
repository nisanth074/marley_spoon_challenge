module MarleySpoon
  class Recipe
    class << self
      def all
        recipe_entries.map do |recipe_entry|
          Recipe.new(recipe_entry)
        end
      end

      def find(id)
        all.detect { |recipe| recipe.id == id }
      end

      private

      def recipe_entries
        MarleySpoon.contentful.entries("content_type" => "recipe")
      end
    end

    attr_reader :recipe_entry

    def initialize(recipe_entry)
      @recipe_entry = recipe_entry
    end

    def id
      recipe_entry.id
    end

    def title
      recipe_entry.fields[:title]
    end

    def photo_url
      recipe_entry.fields[:photo].url
    end

    def description
      recipe_entry.fields[:description]
    end

    def tag_names
      Array(tag_entries).map { |tag_entry| tag_entry.fields[:name] }
    end

    # @todo Add a +Tag+ class
    def tag_entries
      recipe_entry.fields[:tags]
    end

    # @todo Add a +Chef+ class
    def chef_name
      recipe_entry.chef.name if recipe_entry.respond_to?(:chef)
    end
  end
end
