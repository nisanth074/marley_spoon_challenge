module MarleySpoon
  class << self
    def contentful
      @contentful ||= Contentful::Client.new(
        space: ENV["MARLEY_SPOON_CONTENTFUL_SPACE_ID"],
        access_token: ENV["MARLEY_SPOON_CONTENTFUL_ACCESS_TOKEN"]
      )
    end
  end
end
