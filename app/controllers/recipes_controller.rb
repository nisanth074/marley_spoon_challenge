class RecipesController < ApplicationController
  def index
    @recipes = MarleySpoon::Recipe.all.reverse
    @recipe = @recipes.first
  end

  def show
    @recipe = MarleySpoon::Recipe.find(params[:id])
  end
end
